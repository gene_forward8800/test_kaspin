<?php

namespace App\Http\Controllers;

use App\Models\ms_barang;
use App\Models\log_db;
use App\Traits\FlashAlert;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class BarangController extends Controller
{
    use FlashAlert;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $barangs = ms_barang::paginate(10);
        return view('pages.barang.index', compact('barangs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.barang.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (request()->user()->isAbleTo('barang-create')){
            $this->validate($request, [
                'nm_barang' => ['required', 'string', 'max:255'],
                'merek_barang' => ['required', 'string', 'max:255'],
                'status' => ['required'],
            ]);
    
            $tmpData =[
                'nama' => $request->input('nm_barang'),
                'merek' => $request->input('merek_barang'),
                'status' => $request->input('status'),
            ];
    
            if(!empty($request->input('keterangan_barang')) && isset($request->keterangan_barang)){
                $tmpData['keterangan'] = $request->input('keterangan_barang');
            }else{
                $tmpData['keterangan'] = '-';
            }
    
            request()->user()->ms_barang()->create($tmpData);

            log_db::create([
                "id_user_act" => request()->user()->id,
                "note" => "INSERT DATA",
                "data" => json_encode($tmpData),
            ]);

    
            return redirect()->route('barang.index')->with($this->alertCreated());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $barang = ms_barang::findOrFail($id);
            if(!request()->user()){
                return view('pages.barang.edit', compact('barang'));
            }
            
            if (
                request()->user()->isAbleTo(['barang-update','barang-read'], $barang)
                ) {
                return view('pages.barang.edit', compact('barang'));
            } else {
                return redirect()->route('barang.index')->with($this->permissionDenied());
            }
        } catch (ModelNotFoundException $e) {
            return redirect()->route('barang.index')->with($this->alertNotFound());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $barang = ms_barang::findOrFail($id);

            if (
                request()->user()->hasRole(['superadmin', 'admin']) ||
                request()->user()->isAbleTo('barang-update', $barang)
            ) {
                $this->validate($request, [
                    'nm_barang' => ['required', 'string', 'max:255'],
                    'merek_barang' => ['required', 'string', 'max:255'],
                    'status' => ['required'],
                ]);

                $tmpData =[
                    'nama' => $request->input('nm_barang'),
                    'merek' => $request->input('merek_barang'),
                    'status' => $request->input('status'),
                ];

                if(!empty($request->input('keterangan_barang')) && isset($request->keterangan_barang)){
                    $tmpData['keterangan'] = $request->input('keterangan_barang');
                }else{
                    $tmpData['keterangan'] = '-';
                }

                log_db::create([
                    "id_user_act" => request()->user()->id,
                    "note" => "UPDATE DATA",
                    "data" => json_encode($barang),
                ]);
                $barang->update($tmpData);

                return redirect()->route('barang.index')->with($this->alertUpdated());
            } else {
                return redirect()->route('barang.index')->with($this->permissionDenied());
            }
        } catch (ModelNotFoundException $e) {
            return redirect()->route('barang.index')->with($this->alertNotFound());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $barang = ms_barang::findOrFail($id);

            if (
                request()->user()->isAbleTo('barang-delete', $barang)
            ) {
                log_db::create([
                    "id_user_act" => request()->user()->id,
                    "note" => "DELETE DATA",
                    "data" => json_encode($barang),
                ]);
                $barang->delete();

                return redirect()->route('barang.index')->with($this->alertDeleted());
            } else {
                return redirect()->route('barang.index')->with($this->permissionDenied());
            }
        } catch (ModelNotFoundException $e) {
            return redirect()->route('barang.index')->with($this->alertNotFound());
        }
    }
}