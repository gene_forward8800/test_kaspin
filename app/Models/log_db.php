<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class log_db extends Model
{
    use HasFactory;

    protected $connection= 'pgsql';
    protected $table = 'log_barang';
    protected $fillable = ['id_user_act', 'note', 'data'];
}
