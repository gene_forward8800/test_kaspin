<?php

namespace Database\Seeders;

use app\Models\ms_barangs;
use Illuminate\Database\Seeder;

class MsBarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\ms_barang::factory()->count(20)->create(); 
    }
}
