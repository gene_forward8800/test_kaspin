<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ms_barangFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = \App\Models\ms_barang::class;
    
    public function definition()
    {
        
        return [
            'id_user' => rand(1,3),
            'nama' => $this->faker->sentence(1),
            'merek' => $this->faker->sentence(1),
            'keterangan' => $this->faker->paragraph(2),

        ];
    }
}
