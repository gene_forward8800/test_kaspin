<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class MsBarangFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = \App\Models\ms_barang::class;
    
    public function definition()
    {
        
        return [
            'id_user' => rand(1,3),
            'nama' => $faker->sentence(1),
            'merek' => $faker->sentence(1),
            'keterangan' => $faker->paragraph(2),

        ];
    }
}
