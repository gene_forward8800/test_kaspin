@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex justify-content-end">
                <span class="flex-grow-1">Articles</span>
                @permission('barang-create')
                    <a href="{{ route('barang.create') }}" class="btn btn-sm btn-secondary">Create</a>
                @endpermission
                </div>

                <div class="card-body">

                    @if (session('message'))
                    <x-alert :type="session('type')" :message="session('message')" />
                    @endif

                    <table class="table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nama</th>
                                <th>Merek</th>
                                <th>status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($barangs as $barang)
                            <tr>
                                <td>{{ $barang->id }}</td>
                                <td>{{ $barang->nama }}</td>
                                <td>{{ $barang->merek }}</td>
                                <td>{{ $barang->status == 1 ? 'Aktif' : 'Non-Aktif' }}</td>
                                <td>
                                    <form action="{{ route('barang.destroy', $barang->id) }}" method="post">
                                        @method('DELETE') @csrf
                                        <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                                            <a href="{{ route('barang.edit', $barang->id) }}" class="btn btn-info text-white">View</a>
                                        @permission('barang-delete')
                                            <button type="submit" class="btn btn-danger text-white">Delete</button>
                                        @endpermission
                                        </div>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {{ $barangs->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection