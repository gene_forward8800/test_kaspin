@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Barang') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('barang.update', $barang->id) }}">
                        @csrf @method('PUT')

                        <div class="form-group row">
                            <label for="nm_barang" class="col-md-3 col-form-label text-md-right">{{ __('Nama Barang') }}</label>

                            <div class="col-md-7">
                                <input id="nm_barang" type="text" class="form-control @error('nama') is-invalid @enderror" name="nm_barang" value="{{ $barang->nama }}" required autocomplete="nm_barang" placeholder="Nama Barang" autofocus>

                                @error('nama')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="merek_barang" class="col-md-3 col-form-label text-md-right">{{ __('Merek') }}</label>

                            <div class="col-md-7">
                                <input id="merek_barang" type="text" class="form-control @error('merek') is-invalid @enderror" name="merek_barang" value="{{ $barang->merek }}" required autocomplete="merek_barang" placeholder="Merek" autofocus>

                                @error('merek')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="keterangan_barang" class="col-md-3 col-form-label text-md-right">{{ __('Keterangan') }}</label>

                            <div class="col-md-7">
                                <textarea name="keterangan_barang" id="keterangan_barang" cols="30" rows="3" required autocomplete="keterangan_barang" class="form-control @error('keterangan') is-invalid @enderror">{{ $barang->keterangan }}</textarea>

                                @error('keterangan')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="status" class="col-md-3 col-form-label text-md-right">{{ __('Status') }}</label>

                            <div class="col-md-7">
                                <select name="status" id="status" class="form-control @error('status') is-invalid @enderror">
                                        <option value="0" {{ $barang->status == 0 ? 'selected' : ''}}>Tidak Aktif</option>
                                        <option value="1" {{ $barang->status == 1 ? 'selected' : ''}}>Aktif</option>
                                </select>

                                @error('published')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        @role(['admin', 'admin'])


                        @endrole

                        <div class="form-group row mb-0">
                            <div class="col-md-7 offset-md-3">
                            @permission('barang-update')
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Edit') }}
                                </button>
                            @endpermission
                                <a href="{{ route('barang.index') }}" class="btn btn-danger text-white">{{ __('Cancel') }}</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection