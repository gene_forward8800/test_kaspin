<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css" />

    <!-- AOS -->
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    
    <!-- My CSS -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />

</head>

<body id="home">
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary shadow-sm fixed-top" id="navbar">
      <div class="container">
        <a class="navbar-brand" href="#">I Gusti Bagus Ngurah Surya Atmaja</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav ms-auto">
            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="#home">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#about">About</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#projects">Projects</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#contact">Contact</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- Akhir Navbar -->

    <!-- Jumbotron -->
    <section class="jumbotron text-center" id="welcome-section">
      <img src="{{ asset('img/bagus.jpg') }}" alt="Bagus Ngurah" width="200" class="rounded-circle img-thumbnail" />
      <h1 class="display-4">Bagus Ngurah</h1>
      <a href="https://www.instagram.com/bagus8899/" target="_blank" class="btn btn-dark btn-ig mb-1">Follow my Instagram <i class="bi bi-instagram"></i></a>
      <a href="https://www.facebook.com/Bgus.Ngurah" target="_blank" class="btn btn-dark btn-ig">Follow my Facebook <i class="bi bi-facebook"></i></a>
      <p class="lead"></p>
      <svg class="wave1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 200">
        <path
          fill="#ffffff"
          fill-opacity="10"
          d="M0,64L48,80C96,96,192,128,288,128C384,128,480,96,576,80C672,64,768,64,864,96C960,128,1056,192,1152,186.7C1248,181,1344,107,1392,69.3L1440,32L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"
        ></path>
      </svg>
    </section>
    <!-- Akhir Jumbotron -->

    <!-- About -->
    <section id="about">
      <div class="container">
        <div class="row text-center mb-3">
          <div class="col">
            <h2>About Me</h2>
          </div>
        </div>
        <div class="row justify-content-center fs-5 text-justify">
          <div class="col-md-4" data-aos="fade-right" data-aos-delay="200" data-aos-offset="300" data-aos-once="true" data-aos-duration="1000">
            <p>Saya profesional IT dengan pengalaman bekerja lebih dari 10 tahun. <br> <br> Beberapa client yang pernah saya tangani diantaranya RS Bhayangkara Polda Jatim, RSI Jemur Sari, PT Miwon Indonesia, PT PJB Unit Pembangkit berau, app POS pada yukna.id, dll.</p>
          </div>
          <div class="col-md-4" data-aos="fade-left" data-aos-offset="300" data-aos-once="true" data-aos-duration="1000">
            <p>
              Dalam bekerja terdapat beberapa teknologi yang saya gunakan untuk membuat sebuah product diantaranya : <br><br>

              .Net, Vb6, Java, Html, Javascript, Jquery, Css, Bootstrap, Tailwind Css, PHP, Code Igniter, Laravel, Mysql, MS SQL Server, Oracle dan Postgresql.
            </p>
          </div>
        </div>
      </div>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
        <path
          fill="#e2edff"
          fill-opacity="10"
          d="M0,96L48,122.7C96,149,192,203,288,192C384,181,480,107,576,80C672,53,768,75,864,101.3C960,128,1056,160,1152,165.3C1248,171,1344,149,1392,138.7L1440,128L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"
        ></path>
      </svg>
    </section>
    <!-- Akhir About -->

    <!-- Projects -->
    <section id="pengalaman">
      <div class="container">
        <div class="row text-center mb-3">
          <div class="col">
            <h2>Pengalaman Kerja</h2>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="col-md-4 mb-3 d-flex align-items-stretch">
            <div class="card project-tile" data-aos="flip-left" data-aos-offset="300" data-aos-duration="500">
              <img src="{{ asset('img/projects/miwon.png') }}" class="card-img-top" alt="Project 1" />
              <div class="card-body">
                <h5 class="card-title fw-bolder">PT. Miwon Indonesia (2012)</h5>
                <p class="card-text">Pada PT. Miwon Indonesia saya berperan sebagai Staft IT perusahaan. Sebagai seorang staft it saya memiliki tanggung jawab untuk merawat dan mengembangkan sistem ERP yang berjalan di PT. Miwon Indonesia. Selain merawat sistem dan aplikasi sebagai seorang staft IT saya juga memiliki tanggung jawab terkait merawat dan mengembangkan infrastrktur jaringan IT.</p>
              </div>
            </div>
          </div>
          <div class="col-md-4 mb-3 d-flex align-items-stretch">
            <div class="card project-tile" data-aos="flip-left" data-aos-offset="300" data-aos-duration="500" data-aos-delay="100">
              <img src="{{ asset('img/projects/simrs1.jpg') }}" class="card-img-top" alt="Project 2" />
              <div class="card-body">
                <h5 class="card-title fw-bolder">CV. Digital Sense (2012-2016)</h5>
                <p class="card-text">Pada CV. Digital Sense saya berperan sebagai web developer. Sebagai seorang web developer saya diberi kepercayaan untuk mengembangkan dan merawat sistem management rumah sakit yang sudah ada sebelumnya. Selama saya di CV. Digital Sense saya juga diberi kepercayaan untuk menangani beberapa client rumah sakit diantaranya RSUD Sidoarjo, RS Bhayangkara Polda Jatim, RSI Jemur Sari Surabaya dan RSUD Kota Tangerang</p>
              </div>
            </div>
          </div>
          <div class="col-md-4 mb-3 d-flex align-items-stretch">
            <div class="card project-tile" data-aos="flip-left" data-aos-offset="300" data-aos-duration="500" data-aos-delay="200">
              <img src="{{ asset('img/projects/simrs2.jpg') }}" class="card-img-top" alt="Project 3" />
              <div class="card-body">
                <h5 class="card-title fw-bolder">PT. Majapahit Cipta Mandri (2016-2018)</h5>
                <p class="card-text">Pada PT. Majapahit Cipta Mandiri saya berperan sebagai senior web developer. Sebagai senior web developer saya diberi kepercayaan untuk mengembangkan aplikasi enterprise hospital sistem (EHOS) dimana product tersebut sudah berhasil diimplementasikan pada RSUD Pamekasan, RSUD Ibnu Sina Gresik dan RS Siti Khodijah Sidoarjo. </p>
              </div>
            </div>
          </div>
          <div class="col-md-4 mb-3 d-flex align-items-stretch">
            <div class="card project-tile" data-aos="flip-left" data-aos-offset="300" data-aos-duration="500" data-aos-delay="300">
              <img src="{{ asset('img/projects/ct.jpg') }}" class="card-img-top" alt="Project 4" />
              <div class="card-body">
                <h5 class="card-title fw-bolder">PT. Coffee Toffee Indonesia (2018-2020)</h5>
                <p class="card-text">Pada PT. Coffee Toffee Indonesia saya berperan sebagai sistem analis. Sebagai sistem analis saya memiliki tanggung jawab untuk mengembangkan sistem perusahaan, merawat sistem yang sedang berjalan dan memastikan sistem dijalankan dengan benar sesuai degan standart.</p>
              </div>
            </div>
          </div>
          <div class="col-md-4 mb-3 d-flex align-items-stretch">
            <div class="card project-tile" data-aos="flip-left" data-aos-offset="300" data-aos-duration="500" data-aos-delay="400">
              <img src="{{ asset('img/projects/Logo-Yukna-02 (1).png') }}" class="card-img-top" alt="Project 5" />
              <div class="card-body">
                <h5 class="card-title fw-bolder">CV. Bangkit Jayaa (2020-2022)</h5>
                <p class="card-text">
                  Pada CV. Bangkit jaya saya berperan sebagai supervisor dan senior web developer dalam membangun product yukna dimana aplikasi tersebut merupakan ekosistem aplikasi retail. Produduk yukna dapat di akses pada yukna.id.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
        <path
          fill="#ffffff"
          fill-opacity="10"
          d="M0,192L30,208C60,224,120,256,180,245.3C240,235,300,181,360,181.3C420,181,480,235,540,261.3C600,288,660,288,720,272C780,256,840,224,900,218.7C960,213,1020,235,1080,245.3C1140,256,1200,256,1260,250.7C1320,245,1380,235,1410,229.3L1440,224L1440,320L1410,320C1380,320,1320,320,1260,320C1200,320,1140,320,1080,320C1020,320,960,320,900,320C840,320,780,320,720,320C660,320,600,320,540,320C480,320,420,320,360,320C300,320,240,320,180,320C120,320,60,320,30,320L0,320Z"
        ></path>
      </svg>
    </section>
    <!-- Akhir Projects -->

    <!-- Contact -->
    <section id="contact">
      <div class="container">
        <div class="row text-center mb-3">
          <div class="col">
            <h2>Contact Me</h2>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="col-md-6">
            <div class="alert alert-success alert-dismissible fade show d-none my-alert" role="alert">
              <strong>Terimakasih!</strong> Pesan anda sudah kami terima.
              <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            <form name="cv-contact-form">
              <div class="mb-3">
                <label for="name" class="form-label">Nama Lengkap</label>
                <input type="text" class="form-control" required id="name" aria-describedby="name" name="nama" />
              </div>
              <div class="mb-3">
                <label for="email" class="form-label">Email</label>
                <input type="email" class="form-control" id="email" required aria-describedby="email" name="email" />
              </div>
              <div class="mb-3">
                <label for="pesan" class="form-label">Pesan</label>
                <textarea class="form-control" id="pesan" rows="3" name="pesan"></textarea>
              </div>
              <button type="submit" class="btn btn-primary btn-kirim">Kirim</button>

              <button class="btn btn-primary btn-loading d-none" type="button" disabled>
                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                Loading...
              </button>
            </form>
          </div>
        </div>
      </div>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
        <path
          fill="#0d6efd"
          fill-opacity="10"
          d="M0,128L30,149.3C60,171,120,213,180,224C240,235,300,213,360,192C420,171,480,149,540,160C600,171,660,213,720,197.3C780,181,840,107,900,101.3C960,96,1020,160,1080,197.3C1140,235,1200,245,1260,234.7C1320,224,1380,192,1410,176L1440,160L1440,320L1410,320C1380,320,1320,320,1260,320C1200,320,1140,320,1080,320C1020,320,960,320,900,320C840,320,780,320,720,320C660,320,600,320,540,320C480,320,420,320,360,320C300,320,240,320,180,320C120,320,60,320,30,320L0,320Z"
        ></path>
      </svg>
    </section>
    <!-- Akhir Contact -->

    <!-- Footer -->
    <footer class="bg-primary text-white text-center pb-5">
      <p>Created with <i class="bi bi-heart-fill text-danger"></i> by <a href="https://www.instagram.com/bagus8899/" class="text-white fw-bold">Bagus Ngurah</a></p>
    </footer>
    <!-- Akhir Footer -->

    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.9.1/gsap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.9.1/TextPlugin.min.js"></script>
    <script>
      AOS.init();
      gsap.registerPlugin(TextPlugin);
      gsap.to('.lead',{duration: 2, delay: 1.5, text: 'Web Developer | Freelancer'});
      gsap.from('.jumbotron img',{duration: 1, rotateY: 360, opacity: 0});
      gsap.from('.navbar',{duration: 1.5, y: '-100%', opacity: 0, ease: 'bounce'});
      gsap.from('.btn-ig',{duration: 3.5, y: '-100%', opacity: 0, ease: 'bounce'});
      gsap.from('.display-4',{duration: 1, x: -50, opacity: 0, delay: 0.5, ease: 'back'});
    </script>


    <script>
      const scriptURL = 'https://script.google.com/macros/s/AKfycbw8MskKFffQkf4WqkCtO21OwZYE6yLDbE1W-O8jYkLwX5Q1nuWFURrVawGKgww4zRDp/exec';
      const form = document.forms['cv-contact-form'];
      const btnKirim = document.querySelector('.btn-kirim');
      const btnLoading = document.querySelector('.btn-loading');
      const myAlert = document.querySelector('.my-alert');

      form.addEventListener('submit', (e) => {
        e.preventDefault();
        // ketika tombol submit diklik
        // tampilkan tombol loading, hilangkan tombol kirim
        btnLoading.classList.toggle('d-none');
        btnKirim.classList.toggle('d-none');
        fetch(scriptURL, { method: 'POST', body: new FormData(form) })
          .then((response) => {
            // tampilkan tombol kirim, hilangkan tombol loading
            btnLoading.classList.toggle('d-none');
            btnKirim.classList.toggle('d-none');
            // tampilkan alert
            myAlert.classList.toggle('d-none');
            // reset formnya
            form.reset();
            console.log('Success!', response);
          })
          .catch((error) => console.error('Error!', error.message));
      });
    </script>
    <!-- <script src="https://cdn.freecodecamp.org/testable-projects-fcc/v1/bundle.js"></script> -->
  </body>

</html>